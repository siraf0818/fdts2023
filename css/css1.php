<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Latihan 1 : CSS</title>
    <style type="text/css">
        body {
            background-color: lightblue;
        }

        h1 {
            color: white;
            text-align: center;
        }

        p {
            font-family: verdana;
            font-size: 20px;
        }

        .myClass {
            font: bold 1.25em times;
            color: red;
        }

        #myObject1 {
            position: absolute;
            top: 10px;
        }
    </style>
</head>

<body>
    Ini adalah contoh latihan 1 CSS

    <h1>Ini adalah implementasi H1</h1>

    <p>Ini adalah implementasi tag p / paragraf</p>
    <h1 class="myClass">Ini adalah H1 myclass</h1>
    <p class="myClass">Ini adalah P myclass</p>
    <h1 id=”myObject”> …. </h1>
</body>

</html>
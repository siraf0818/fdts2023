<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Latihan 7 Javascript : Variable</title>
	<script type="text/javascript">
		//membuat variable
		var name = "Faris Adhi";
		var visitorCount = 818;
		var isActive = true;
		var url = "https://polines.ac.id";

		//menampilkan variable di jendela dialog (alert)
		alert("Selamat datang di website : " + name);

		//menampilkan variable ke dalam HTML
		document.write("Nama situs : " + name + "<br>");
		document.write("Jumlah Pengunjung : " + visitorCount + "<br>");
		document.write("Status aktif : " + isActive + "<br>");
		document.write("Alamat URL : " + url + "<br>");
	</script>
</head>

<body>
	<?php
	$nama = "Faris";
	$nama2 = "Yusuf";
	$nama_gabung = $nama . " " . $nama2;
	echo $nama_gabung;
	?>
</body>

</html>